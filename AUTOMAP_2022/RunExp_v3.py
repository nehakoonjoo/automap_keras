
from BZLIBS import SQLconnect as sq
from shutil import copyfile
from collections import namedtuple
import os
import importlib
import socket


this_machine = socket.gethostname()

# connect to database
sqldb = 'automap'
db = sq.DbConnect(sqldb)

run_flag = 1

while run_flag==1:

    if this_machine == 'vulcan':
        machine_table = 'vulcan_exps'
    elif this_machine == 'lfibrainlinux':
        machine_table = 'brain_exps'
    elif this_machine == 'lfibraindocker':
        machine_table = 'brain_exps'
    elif this_machine == 'GAMERDOCKER':
        machine_table = 'gamer_exps'
    elif this_machine == 'emiccdspm3':
        machine_table = 'p100_exps'
    elif this_machine == 'DGXBZ':
        machine_table = 'dgx_exps'
    elif this_machine == 'trabant.nmr.mgh.harvard.edu':
        machine_table = 'trabant_exps'
    elif this_machine == 'tehgamer':
        machine_table = 'tehgamer_exps'
    elif this_machine == 'Neon':
        machine_table = 'Neon_exps'
    elif this_machine == 'selina':
        machine_table = 'selina_exps'
    elif this_machine == 'mitdaidp01':
        machine_table = 'lms_exps'

    if sq.DBCheckNull(db,machine_table,'status',2)=='NONE':
        run_flag = 0
        break
    # get info from db

    db_machine = sq.DBReadRow(db,machine_table,'machine',2)
    db_script_file = sq.DBReadRow(db,machine_table,'script_file',2)
    db_arch_file = sq.DBReadRow(db,machine_table,'arch_file',2)
    db_trajectory = sq.DBReadRow(db,machine_table,'trajectory',2)
    db_resolution = sq.DBReadRow(db,machine_table,'resolution',2)
    db_arch_desc = sq.DBReadRow(db,machine_table,'arch_desc',2)
    db_train_desc = sq.DBReadRow(db,machine_table,'train_desc',2)
    db_test_desc = sq.DBReadRow(db,machine_table,'test_desc',2)

    db_train_in_file = sq.DBReadRow(db,machine_table,'train_in_file',2)
    db_train_out_file = sq.DBReadRow(db,machine_table,'train_out_file',2)
    db_test_in_file = sq.DBReadRow(db,machine_table,'test_in_file',2)
    db_test_out_file = sq.DBReadRow(db,machine_table,'test_out_file',2)
    db_in_dim = sq.DBReadRow(db,machine_table,'in_dim',2)
    db_h_dim = sq.DBReadRow(db, machine_table, 'h_dim', 2)
    db_out_dim = sq.DBReadRow(db,machine_table,'out_dim',2)
    db_batch_size = sq.DBReadRow(db,machine_table,'batch_size',2)
    db_n_epochs = sq.DBReadRow(db,machine_table,'n_epochs',2)
    db_c_prob = sq.DBReadRow(db,machine_table,'c_prob',2)
    db_l_rate = sq.DBReadRow(db,machine_table,'l_rate',2)
    db_model_in_trueid = sq.DBReadRow(db,machine_table,'model_in_trueid',2)
    db_model_vars_in = sq.DBReadRow(db,machine_table,'model_vars_in',2)
    db_trainable_model_in = sq.DBReadRow(db, machine_table, 'trainable_model_in', 2)
    db_data_dir= sq.DBReadRow(db,machine_table,'data_dir',2)
    db_notes = sq.DBReadRow(db,machine_table,'notes',2)
    db_out_dir = sq.DBReadRow(db, machine_table, 'out_dir', 2)
    db_scp_dir = sq.DBReadRow(db, machine_table, 'scp_dir', 2)
    db_precision = sq.DBReadRow(db, machine_table, 'fp_prec', 2)


    #scriptfile = 'script_db.py'
    scriptfile = db_script_file
    # import script name
    scriptname = scriptfile[:-3]
    scr = importlib.import_module('Scripts.'+scriptname)


    # export db info to script file

    scr.import_exp_vars(in_machine=db_machine, in_trajectory=db_trajectory, in_resolution=db_resolution,
                        in_arch_desc=db_arch_desc, in_arch_file=db_arch_file, in_train_desc=db_train_desc,
                        in_test_desc=db_test_desc, in_data_dir=db_data_dir, in_train_in=db_train_in_file,
                        in_train_out=db_train_out_file, in_test_in=db_test_in_file, in_test_out=db_test_out_file,
                        in_model_in_trueid=db_model_in_trueid, in_model_vars_in=db_model_vars_in, in_trainable_model_in=db_trainable_model_in,
                        in_in_dim=db_in_dim, in_h_dim=db_h_dim, in_out_dim=db_out_dim, in_n_epochs=db_n_epochs, in_batch_size=db_batch_size,
                        in_c_prob=db_c_prob, in_l_rate=db_l_rate, in_notes=db_notes, in_out_dir = db_out_dir, in_scp_dir=db_scp_dir,
                        in_precision = db_precision)

    # get machine from script (needed for db initalization)
    machine = scr.get_machine()

    # <editor-fold desc="Directories">
    # long term directories
    if this_machine == 'vulcan':
        out_dir = '/home/lfi/StoreData/AUTOMAP/'
        script_dir = '/home/lfi/Source/automap/Scripts/'
        arch_dir = '/home/lfi/Source/automap/Architectures/'

    elif this_machine == 'lfibrainlinux':
        out_dir = '/home/lfibrain/StoreData/AUTOMAP/'
        script_dir = '/home/lfibrain/Source/automap/Scripts/'
        arch_dir = '/home/lfibrain//Source/automap/Architectures/'

    elif this_machine == 'lfibraindocker':
        out_dir = '/StoreData/AUTOMAP/'
        script_dir = '/Source/automap/Scripts/'
        arch_dir = '/Source/automap/Architectures/'

    elif this_machine == 'GAMERDOCKER':
        out_dir = '/StoreData/AUTOMAP/'
        script_dir = '/Source/automap/Scripts/'
        arch_dir = '/Source/automap/Architectures/'

    elif this_machine == 'emiccdspm3':
        out_dir = '/home/administrator/StoreData/AUTOMAP/'
        script_dir = '/home/administrator/Source/automap/Scripts/'
        arch_dir = '/home/administrator/Source/automap/Architectures/'

    elif this_machine == 'DGXBZ':
        out_dir = '/hdd/misc_hdd/StoreData/AUTOMAP/'
        script_dir = '/home/lfi/Source/automap/Scripts/'
        arch_dir = '/home/lfi/Source/automap/Architectures/'

    elif this_machine == 'trabant.nmr.mgh.harvard.edu':
        out_dir = '/homes/6/bzhu/StoreData/AUTOMAP/'
        script_dir = '/homes/6/bzhu/Source/automap/Scripts/'
        arch_dir = '/homes/6/bzhu/Source/automap/Architectures/'

        # out_dir = '/data/scratch/nk771/StoreData/AUTOMAP/'
        # script_dir = '/homes/8/nk771/Source/automap/automap/Scripts/'
        # arch_dir = '/homes/8/nk771/Source/automap/automap/Architectures/'

    elif this_machine == 'tehgamer':

        if scriptname[0] == 'k':
            out_dir = '/home/lfi/misc_hdd/StoreData/AUTOMAP/'
            script_dir = '/home/lfi/Source/automap_keras/AUTOMAP_2022/Scripts/'
            arch_dir = '/home/lfi/Source/automap_keras/AUTOMAP_2022/models/'
        else:
            out_dir = '/home/lfi/misc_hdd/StoreData/AUTOMAP/'
            script_dir = '/home/lfi/Source/automap/Scripts/'
            arch_dir = '/home/lfi/Source/automap/Architectures/'

    elif this_machine == 'Neon':
        # out_dir = '/home/lfi/Data/StoreData/AUTOMAP/'
        out_dir = '/hdd1/StoreData/AUTOMAP/'
        script_dir = '/home/lfi/Source/automap/Scripts/'
        arch_dir = '/home/lfi/Source/automap/Architectures/'

    elif this_machine == 'selina':
        out_dir = '/home/lfi/Data/StoreData/AUTOMAP/'
        script_dir = '/home/lfi/workspace/automap/Scripts/'
        arch_dir = '/home/lfi/workspace/automap/Architectures/'

    elif this_machine == 'mitdaidp01':
        out_dir = '/home/bzhu/poc_data_folder/StoreData/automap/'
        script_dir = '/home/lfi/Source/automap/Scripts/'
        arch_dir = '/home/lfi/Source/automap/Architectures/'

    # </editor-fold>

    # <editor-fold desc="MySQL DB Initialization">



    # update experiment count
    sq.UpdateExpCount(db,machine)

    # extract date, count, experiment row
    curdate = sq.GetCurDate(db)
    curtime = sq.GetCurTime(db)
    curcount = sq.GetCurExpCount(db,machine)
    curexprow = sq.GetCurExpRow(db)
    trueid = str(curdate) + '_' + machine[0:2] + '_' + str(curcount).zfill(3)

    # initialize db row for experiment
    sq.InitExpRow(db)

    # write front matter to experiment db
    sq.WriteToDB(db,'experiments',curexprow,'machine',machine)
    sq.WriteToDB(db,'experiments',curexprow,'date',curdate)
    sq.WriteToDB(db,'experiments',curexprow,'start_time',curtime)
    sq.WriteToDB(db,'experiments',curexprow,'daycount',curcount)
    sq.WriteToDB(db,'experiments',curexprow,'trueid',trueid)
    sq.WriteToDB(db,'experiments',curexprow,'script_file',scriptfile[:-3])

    # </editor-fold>

    # write "RUNNING" to nextexp db
    sq.WriteToDBSecondRow(db,machine_table,'status','RUNNING') #insert "RUNNING" into status of second row

    #scr.test_changing_var()

    # Initialize experiment
    exp_info_out = scr.initialize_test(trueid)

    # <editor-fold desc="Create Exp Folder and Script+Arch Files">
    # create directory
    exp_out_dir = out_dir+trueid
    if not os.path.exists(exp_out_dir):
        os.makedirs(exp_out_dir)

    # copy script
    fullscriptfile = script_dir+scriptfile
    #print(fullscriptfile)
    fullscriptdest = exp_out_dir+'/'+scriptfile
    print('HERE', fullscriptdest)

    # copy arch file
    fullarchfile = arch_dir + exp_info_out.arch_file
    fullarchdest = exp_out_dir+'/'+exp_info_out.arch_file


    copyfile(fullscriptfile, fullscriptdest)
    copyfile(fullarchfile, fullarchdest)
    # </editor-fold>


    # <editor-fold desc="DB Write Script Params">
    sq.WriteToDB(db,'experiments',curexprow,'arch_file',exp_info_out.arch_file)
    sq.WriteToDB(db,'experiments',curexprow,'completed',exp_info_out.completed)
    sq.WriteToDB(db,'experiments',curexprow,'trajectory',exp_info_out.trajectory)
    sq.WriteToDB(db,'experiments',curexprow,'resolution',exp_info_out.resolution)
    sq.WriteToDB(db,'experiments',curexprow,'completed',exp_info_out.completed)
    sq.WriteToDB(db,'experiments',curexprow,'arch_desc',exp_info_out.arch_desc)
    sq.WriteToDB(db,'experiments',curexprow,'train_in_file',exp_info_out.train_in)
    sq.WriteToDB(db,'experiments',curexprow,'train_out_file',exp_info_out.train_out)
    sq.WriteToDB(db,'experiments',curexprow,'test_in_file',exp_info_out.test_in)
    sq.WriteToDB(db,'experiments',curexprow,'test_out_file',exp_info_out.test_out)
    sq.WriteToDB(db,'experiments',curexprow,'in_dim',exp_info_out.in_dim)
    sq.WriteToDB(db,'experiments',curexprow,'h_dim', exp_info_out.h_dim)
    sq.WriteToDB(db,'experiments',curexprow,'out_dim',exp_info_out.out_dim)
    sq.WriteToDB(db,'experiments',curexprow,'batch_size',exp_info_out.batch_size)
    sq.WriteToDB(db,'experiments',curexprow,'n_epochs',exp_info_out.n_epochs)
    sq.WriteToDB(db,'experiments',curexprow,'c_prob',exp_info_out.c_prob)
    sq.WriteToDB(db,'experiments',curexprow,'l_rate',exp_info_out.l_rate)
    #sq.WriteToDB(db,'experiments',curexprow,'train_flag',exp_info_out.train_flag)
    sq.WriteToDB(db,'experiments',curexprow,'model_in_trueid',exp_info_out.model_in_trueid)
    sq.WriteToDB(db,'experiments',curexprow,'model_vars_in',exp_info_out.model_vars_in)
    sq.WriteToDB(db, 'experiments', curexprow, 'trainable_model_in', exp_info_out.trainable_model_in)
    sq.WriteToDB(db,'experiments',curexprow,'train_desc',exp_info_out.train_desc)
    sq.WriteToDB(db,'experiments',curexprow,'test_desc',exp_info_out.test_desc)
    sq.WriteToDB(db,'experiments',curexprow,'data_dir',exp_info_out.data_dir)
    sq.WriteToDB(db, 'experiments', curexprow, 'notes', exp_info_out.notes)
    sq.WriteToDB(db, 'experiments', curexprow, 'fp_prec', exp_info_out.precision)
    # </editor-fold>


    cost_dict = scr.train_model(trueid)

    train_cost = "{:.2e}".format(cost_dict['Train_cost'])
    val_cost = "{:.2e}".format(cost_dict['Val_cost'])

    # update experiments table at end
    curtime = sq.GetCurTime(db)
    sq.WriteToDB(db,'experiments',curexprow,'completed',1)
    sq.WriteToDB(db,'experiments',curexprow,'end_time',curtime)
    sq.WriteToDB(db, 'experiments', curexprow, 'final_train_cost', train_cost)
    sq.WriteToDB(db, 'experiments', curexprow, 'final_val_cost', val_cost)

    # update nextexp table at end

    sq.DeleteFirstRow(db,machine_table) #delete first now
    sq.WriteToDBFirstRow(db,machine_table,'status','DONE') #insert "DONE" into status of updated first row



