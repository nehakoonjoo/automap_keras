import MySQLdb

sqlhost = "172.21.21.69"
hostuser = "lfi"
hostpw = "lfilab2.0"

def DbConnect(sqldb):

    db = MySQLdb.connect(sqlhost, hostuser, hostpw, sqldb)

    return db

def DBCheckNull(db,table,column,rownum):
    with db:
        cur = db.cursor()
        cur.execute("SELECT IFNULL( (SELECT " + column + " FROM " + table + " LIMIT " + str(rownum - 1) + ",1), 'NONE');")
        readinfo = cur.fetchall()[0][0]
    return readinfo

def DBReadRow(db,table,column,rownum):

    with db:
        cur = db.cursor()
        cur.execute("SELECT " + column + " FROM " + table + " LIMIT " + str(rownum-1) + ",1;")
        readinfo = cur.fetchall()[0][0]

    return readinfo

def GetCurDate(db):


    # machine = 'Brain'
    with db:
        cur = db.cursor()

        cur.execute("SELECT CURDATE();")
        cur_date = cur.fetchall()[0][0]

    return cur_date

def GetCurTime(db):


    # machine = 'Brain'
    with db:
        cur = db.cursor()

        cur.execute("SELECT NOW();")
        cur_time = cur.fetchall()[0][0]

    return cur_time

def GetCurExpCount(db,machine):

    with db:
        cur = db.cursor()

        if machine == 'Vulcan':
            cur.execute("SELECT v_count FROM expcounter;")
            curcount = cur.fetchall()[0][0]

        elif machine == 'Brain':
            cur.execute("SELECT b_count FROM expcounter;")
            curcount = cur.fetchall()[0][0]

        elif machine == 'P100':
            cur.execute("SELECT p_count FROM expcounter;")
            curcount = cur.fetchall()[0][0]

        elif machine == 'DGX':
            cur.execute("SELECT d_count FROM expcounter;")
            curcount = cur.fetchall()[0][0]

        elif machine == 'GAMERDOCKER':
            cur.execute("SELECT g_count FROM expcounter;")
            curcount = cur.fetchall()[0][0]
            
        elif machine == 'tehgamer':
            cur.execute("SELECT tg_count FROM expcounter;")
            curcount = cur.fetchall()[0][0]

        elif machine == 'tehgamerkeras':
            cur.execute("SELECT tgk_count FROM expcounter;")
            curcount = cur.fetchall()[0][0]

        elif machine == 'Neon':
            cur.execute("SELECT n_count FROM expcounter;")
            curcount = cur.fetchall()[0][0]

        elif machine == 'trabant.nmr.mgh.harvard.edu':
            cur.execute("SELECT t_count FROM expcounter;")
            curcount = cur.fetchall()[0][0]

        elif machine == 'lms':
            cur.execute("SELECT l_count FROM expcounter;")
            curcount = cur.fetchall()[0][0]

    return curcount

def GetCurExpRow(db):

    with db:
        cur = db.cursor()
        cur.execute("SELECT id FROM experiments ORDER BY id DESC LIMIT 1;")
        numrows = cur.fetchall()[0][0]
        thisexprow = numrows+1

    return thisexprow


def UpdateExpCount(db,machine):

    with db:
        cur = db.cursor()

        cur.execute("SELECT CURDATE();")
        cur_date = cur.fetchall()[0][0]

        if machine == 'Vulcan':

            cur.execute("SELECT v_day FROM expcounter;")
            v_day_in_table = cur.fetchall()[0][0]

            if v_day_in_table==cur_date:
                # get current count
                cur.execute("SELECT v_count FROM expcounter;")
                v_count = cur.fetchall()[0][0]
                # increment count
                cur.execute("UPDATE expcounter SET v_count=" + str(v_count+1) + ";")
                #cur.execute("SELECT v_count FROM expcounter;")
            else:
                # update date to current date
                cur.execute("UPDATE expcounter SET v_day = CURDATE()")
                # reset counter to zero
                cur.execute("UPDATE expcounter SET v_count=1")

        elif machine == 'Brain':

            cur.execute("SELECT b_day FROM expcounter;")
            b_day_in_table = cur.fetchall()[0][0]

            if b_day_in_table==cur_date:
                # get current count
                cur.execute("SELECT b_count FROM expcounter;")
                b_count = cur.fetchall()[0][0]
                # increment count
                cur.execute("UPDATE expcounter SET b_count=" + str(b_count + 1) + ";")
                #cur.execute("SELECT b_count FROM expcounter;")
            else:
                # update date to current date
                cur.execute("UPDATE expcounter SET b_day = CURDATE()")
                # reset counter to zero
                cur.execute("UPDATE expcounter SET b_count = 1")

        elif machine == 'P100':

            cur.execute("SELECT p_day FROM expcounter;")
            p_day_in_table = cur.fetchall()[0][0]

            if p_day_in_table == cur_date:
                # get current count
                cur.execute("SELECT p_count FROM expcounter;")
                p_count = cur.fetchall()[0][0]
                # increment count
                cur.execute("UPDATE expcounter SET p_count=" + str(p_count + 1) + ";")
                # cur.execute("SELECT p_count FROM expcounter;")
            else:
                # update date to current date
                cur.execute("UPDATE expcounter SET p_day = CURDATE()")
                # reset counter to zero
                cur.execute("UPDATE expcounter SET p_count = 1")

        elif machine == 'DGX':

            cur.execute("SELECT d_day FROM expcounter;")
            d_day_in_table = cur.fetchall()[0][0]

            if d_day_in_table == cur_date:
                # get current count
                cur.execute("SELECT d_count FROM expcounter;")
                d_count = cur.fetchall()[0][0]
                # increment count
                cur.execute("UPDATE expcounter SET d_count=" + str(d_count + 1) + ";")
                # cur.execute("SELECT d_count FROM expcounter;")
            else:
                # update date to current date
                cur.execute("UPDATE expcounter SET d_day = CURDATE()")
                # reset counter to zero
                cur.execute("UPDATE expcounter SET d_count = 1")

        elif machine == 'GAMERDOCKER':

            cur.execute("SELECT g_day FROM expcounter;")
            g_day_in_table = cur.fetchall()[0][0]

            if g_day_in_table == cur_date:
                # get current count
                cur.execute("SELECT g_count FROM expcounter;")
                g_count = cur.fetchall()[0][0]
                # increment count
                cur.execute("UPDATE expcounter SET g_count=" + str(g_count + 1) + ";")
                # cur.execute("SELECT g_count FROM expcounter;")
            else:
                # update date to current date
                cur.execute("UPDATE expcounter SET g_day = CURDATE()")
                # reset counter to zero
                cur.execute("UPDATE expcounter SET g_count = 1")
        
        elif machine == 'tehgamer':

            cur.execute("SELECT tg_day FROM expcounter;")
            tg_day_in_table = cur.fetchall()[0][0]

            if tg_day_in_table == cur_date:
                # get current count
                cur.execute("SELECT tg_count FROM expcounter;")
                tg_count = cur.fetchall()[0][0]
                # increment count
                cur.execute("UPDATE expcounter SET tg_count=" + str(tg_count + 1) + ";")
                # cur.execute("SELECT tg_count FROM expcounter;")
            else:
                # update date to current date
                cur.execute("UPDATE expcounter SET tg_day = CURDATE()")
                # reset counter to zero
                cur.execute("UPDATE expcounter SET tg_count = 1")

        elif machine == 'tehgamerkeras':

            cur.execute("SELECT tgk_day FROM expcounter;")
            tgk_day_in_table = cur.fetchall()[0][0]

            if tgk_day_in_table == cur_date:
                # get current count
                cur.execute("SELECT tgk_count FROM expcounter;")
                tgk_count = cur.fetchall()[0][0]
                # increment count
                cur.execute("UPDATE expcounter SET tgk_count=" + str(tgk_count + 1) + ";")
                # cur.execute("SELECT tgk_count FROM expcounter;")
            else:
                # update date to current date
                cur.execute("UPDATE expcounter SET tgk_day = CURDATE()")
                # reset counter to zero
                cur.execute("UPDATE expcounter SET tgk_count = 1")

        elif machine == 'Neon':

            cur.execute("SELECT n_day FROM expcounter;")
            n_day_in_table = cur.fetchall()[0][0]

            if n_day_in_table == cur_date:
                # get current count
                cur.execute("SELECT n_count FROM expcounter;")
                n_count = cur.fetchall()[0][0]
                # increment count
                cur.execute("UPDATE expcounter SET n_count=" + str(n_count + 1) + ";")
                # cur.execute("SELECT n_count FROM expcounter;")
            else:
                # update date to current date
                cur.execute("UPDATE expcounter SET n_day = CURDATE()")
                # reset counter to zero
                cur.execute("UPDATE expcounter SET n_count = 1")

        elif machine == 'trabant.nmr.mgh.harvard.edu':

            cur.execute("SELECT t_day FROM expcounter;")
            t_day_in_table = cur.fetchall()[0][0]

            if t_day_in_table == cur_date:
                # get current count
                cur.execute("SELECT t_count FROM expcounter;")
                t_count = cur.fetchall()[0][0]
                # increment count
                cur.execute("UPDATE expcounter SET t_count=" + str(t_count + 1) + ";")
                # cur.execute("SELECT t_count FROM expcounter;")
            else:
                # update date to current date
                cur.execute("UPDATE expcounter SET t_day = CURDATE()")
                # reset counter to zero
                cur.execute("UPDATE expcounter SET t_count = 1")

        elif machine == 'lms':

            cur.execute("SELECT l_day FROM expcounter;")
            l_day_in_table = cur.fetchall()[0][0]

            if l_day_in_table == cur_date:
                # get current count
                cur.execute("SELECT l_count FROM expcounter;")
                l_count = cur.fetchall()[0][0]
                # increment count
                cur.execute("UPDATE expcounter SET l_count=" + str(l_count + 1) + ";")
                # cur.execute("SELECT l_count FROM expcounter;")
            else:
                # update date to current date
                cur.execute("UPDATE expcounter SET l_day = CURDATE()")
                # reset counter to zero
                cur.execute("UPDATE expcounter SET l_count = 1")

        # result = cur.fetchall()
        # for r in result:
        #      print(r[0])

def InitExpRow(db):

    with db:
        cur = db.cursor()
        cur.execute("INSERT INTO experiments (machine) VALUES ('NULL')")

def WriteToDB(db,table,rowid,col,value):

    with db:
        cur = db.cursor()
        cur.execute("UPDATE " + table + " SET " + col + " = '" + str(value) + "' WHERE id = " + str(rowid))

def DeleteFirstRow(db,table):

    with db:
        cur = db.cursor()
        cur.execute("SELECT id FROM " + table + " ORDER BY id ASC LIMIT 1;")
        id_firstrow = cur.fetchall()[0][0]
        cur.execute("DELETE FROM " + table + " WHERE id =" + str(id_firstrow) + " LIMIT 1;")

def WriteToDBFirstRow(db,table,col,value):

    with db:
        cur = db.cursor()
        cur.execute("SELECT id FROM " + table + " ORDER BY id ASC LIMIT 1;")
        id_firstrow = cur.fetchall()[0][0]
        cur.execute("UPDATE " + table + " SET " + col + " = '" + str(value) + "' WHERE id =" + str(id_firstrow) + ";")

def WriteToDBSecondRow(db,table,col,value):

    with db:
        cur = db.cursor()
        cur.execute("SELECT id FROM " + table + " ORDER BY id ASC LIMIT 1,1;")
        id_secondrow = cur.fetchall()[0][0]
        cur.execute("UPDATE " + table + " SET " + col + " = '" + str(value) + "' WHERE id =" + str(id_secondrow) + ";")