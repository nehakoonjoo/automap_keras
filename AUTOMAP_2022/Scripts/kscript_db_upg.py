

import tensorflow as tf
from collections import namedtuple
# from BZLIBS import h5py_fns as h5bz

import paramiko
import os
import scipy.io as sio
import numpy as np
import math
import time
import h5py
import importlib
import csv
import ast
#from libs.activations import lrelu
#from libs.utils import corrupt

import tensorflow as tf

from data_loader.automap_data_generator import DataGenerator, ValDataGenerator
from models.automap_model import AUTOMAP_Basic_Model
from trainers.automap_trainer import AUTOMAP_Trainer
from utils.config import process_config
from utils.dirs import create_dirs
from utils.utils import get_args

from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import init_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import nn
from tensorflow.python.ops import standard_ops
from tensorflow.python.ops import variable_scope
# slim = tf.contrib.slim

os.environ["CUDA_VISIBLE_DEVICES"]="0,1"

machine = 'Vulcan'
trajectory = 'Cartesian'
resolution = 64
arch_desc = '2FC only'

arch_file = 'test_arch.py'
train_desc = 'cart64 hcp no-phase'
test_desc = 'cart64 hcp no-phase'

data_dir = ''
train_in = 'train_fft_x_hcp_nophase_64.mat'
train_out = 'train_x_hcp_nophase_64.mat'
test_in = 'test_fft_x_hcp_nophase_64.mat'
test_out = 'test_x_hcp_nophase_64.mat'


#model_in_trueid = '2016-09-12_V_147'
model_in_trueid = None

model_vars_in = 'All'
#model_vars_in = ['W1_fc','b1_fc']

trainable_model_in = 'True'

in_dim = resolution**2 * 2
h_dim = in_dim
out_dim = resolution**2

n_epochs = 49
batch_size = 500

c_prob = 1
l_rate = 0.005
notes = ''

precision = 'FP32'

out_dir = ''
scp_dir = ''
log_filename = ''
csv_filename = ''
exp_out_dir = ''

keep_prob = 1.0


def get_machine():
    return machine

def import_exp_vars(in_machine, in_trajectory, in_resolution, in_arch_desc, in_arch_file, in_train_desc, in_test_desc,
                      in_data_dir, in_train_in, in_train_out, in_test_in, in_test_out, in_model_in_trueid, in_model_vars_in, in_trainable_model_in, in_in_dim, in_h_dim, in_out_dim,
                      in_n_epochs, in_batch_size, in_c_prob, in_l_rate, in_notes, in_out_dir, in_scp_dir, in_precision):
    global machine
    global trajectory
    global resolution
    global arch_desc

    global arch_file
    global train_desc
    global test_desc

    global data_dir
    global train_in
    global train_out
    global test_in
    global test_out


    global model_in_trueid
    global model_vars_in
    global trainable_model_in

    global in_dim
    global h_dim
    global out_dim

    global n_epochs
    global batch_size

    global c_prob
    global l_rate
    global notes
    global scp_dir
    global out_dir
    global precision

    machine = in_machine
    trajectory = in_trajectory
    resolution = in_resolution
    arch_desc = in_arch_desc

    arch_file = in_arch_file
    train_desc = in_train_desc
    test_desc = in_test_desc

    data_dir = in_data_dir
    train_in = in_train_in
    train_out = in_train_out
    test_in = in_test_in
    test_out = in_test_out

    model_in_trueid = in_model_in_trueid
    model_vars_in = in_model_vars_in
    trainable_model_in = in_trainable_model_in

    in_dim = in_in_dim
    h_dim = in_h_dim
    out_dim = in_out_dim

    n_epochs = in_n_epochs
    batch_size = in_batch_size

    c_prob = in_c_prob
    l_rate = in_l_rate
    notes = in_notes
    scp_dir = in_scp_dir
    out_dir = in_out_dir

    precision = in_precision



def initialize_test(trueid_from_run):

    trueid = trueid_from_run
    exp_out_dir = out_dir+trueid+'/'

    completed = 0

    info_out = namedtuple('info_out', 'arch_file completed trajectory resolution arch_desc train_desc test_desc data_dir '
                                      'train_in train_out test_in test_out in_dim h_dim out_dim batch_size n_epochs c_prob l_rate'
                                      ' model_in_trueid model_vars_in trainable_model_in notes precision')
    exp_info_out = info_out(arch_file, completed, trajectory, resolution, arch_desc, train_desc, test_desc, data_dir, train_in,
                            train_out, test_in, test_out, in_dim, h_dim, out_dim, batch_size, n_epochs,c_prob, l_rate,
                            model_in_trueid, model_vars_in, trainable_model_in, notes, precision)

    return exp_info_out


def train_model(trueid):

    if model_in_trueid == 'None':
        args = get_args()
        config = process_config(args.config)

        create_dirs([config.summary_dir, config.checkpoint_dir])

        print(config.summary_dir)

        data = DataGenerator(config)
        valdata = ValDataGenerator(config)

        if config.resume == 0:
            model = AUTOMAP_Basic_Model(config)
        elif config.resume == 1:
            model = tf.keras.models.load_model(config.loadmodel_dir)
        model.summary()

        trainer = AUTOMAP_Trainer(model, data, valdata, config)
        trainer.train()


    # # <editor-fold desc="Model Initialization">
    # archname = arch_file[:-3]
    # arch = importlib.import_module('Architectures.' + archname)
    #
    # if model_in_trueid == 'None':
    #
    #
    #
    #     net = arch.network(batch_size, precision, resolution, in_dim, h_dim, out_dim, model_in_vars = [], model_in_shapes = [], trainable_model_in = [])
    #
    #     # optimizer = tf.train.AdagradOptimizer(l_rate).minimize(net['cost'], colocate_gradients_with_ops=True)
    #     optimizer = tf.compat.v1.train.RMSPropOptimizer(l_rate).minimize(net['cost'])
    #
    #     train_op = optimizer
    #
    #     config = tf.compat.v1.ConfigProto(allow_soft_placement=True)
    #     sess = tf.compat.v1.Session(config=config)
    #     sess.run(tf.compat.v1.initialize_all_variables())
    #
    #
    #
    #
    # elif model_vars_in == 'All':
    #
    #     model_data = []
    #     model_shape = []
    #     full_datasets = []
    #     net_in = []
    #
    #     if '[' not in model_in_trueid:
    #         modellist = [model_in_trueid]
    #     else:
    #         modellist = ast.literal_eval(model_in_trueid)
    #
    #     for m in modellist:
    #
    #         if os.path.isdir(out_dir + m) == False:
    #             os.mkdir(out_dir + m)
    #             server = '172.21.21.69'
    #             username = 'lfi'
    #             password = 'lfilab2.0'
    #             ssh = paramiko.SSHClient()
    #             ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")))
    #             ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    #             ssh.connect(server, username=username, password=password)
    #             sftp = ssh.open_sftp()
    #             # if no folder, create folder
    #             remote_path = scp_dir + m + '/'
    #             try:
    #                 sftp.chdir(remote_path)  # Test if remote_path exists
    #             except IOError:
    #                 sftp.mkdir(remote_path)  # Create remote_path
    #                 sftp.chdir(remote_path)
    #
    #             filelist = sftp.listdir(remote_path)
    #
    #             for filenum in range(len(filelist)):
    #                 print(filelist[filenum])
    #                 sftp.get(filelist[filenum], out_dir + m + '/' + filelist[filenum])
    #
    #             sftp.close()
    #             ssh.close()
    #
    #         with h5py.File(out_dir + m + '/' + m + '.h5', 'r') as hf_in:
    #             datasets = h5bz.get_datasets(out_dir + m + '/' + m + '.h5')
    #
    #             for param in range(len(datasets)):
    #                 full_datasets.append(datasets[param])
    #                 h5data = hf_in.get(datasets[param])
    #                 h5data_np = np.array(h5data)
    #                 print('====h5py LOADED WEIGHTS=====')
    #                 print(datasets[param])
    #                 model_data.append(h5data_np)
    #                 model_shape.append(h5data_np.shape)
    #
    #     net = arch.network(batch_size, precision, resolution, in_dim, h_dim, out_dim, model_in_vars=full_datasets,
    #                        model_in_shapes=model_shape, trainable_model_in=trainable_model_in)
    #
    #     for param in range(len(full_datasets)):
    #         net_in.append(net[full_datasets[param]])
    #
    #     print('=====FAST TRAINING VARS=======')
    #     print(net['Fast_train_vars'])
    #     print('=====SLOW TRAINING VARS=======')
    #     print(net['Slow_train_vars'])
    #
    #     print('====TRAINABLE VARIABLES IN SCRIPT_DB====')
    #     for var in range(len(tf.compat.v1.trainable_variables())):
    #         print(tf.compat.v1.trainable_variables()[var].name.split(':')[0])
    #
    #     if len(net['Fast_train_vars']) != 0:
    #         # optimizer1 = tf.train.AdagradOptimizer(l_rate).minimize(net['cost'], var_list=net['Fast_train_vars'],
    #         #                                                         colocate_gradients_with_ops=True)
    #         optimizer1 = tf.compat.v1.train.RMSPropOptimizer(l_rate).minimize(net['cost'], var_list=net['Fast_train_vars'])
    #
    #     if len(net['Slow_train_vars']) != 0:
    #         # optimizer2 = tf.train.AdagradOptimizer(l_rate / 10).minimize(net['cost'], var_list=net['Slow_train_vars'],
    #         #                                                             colocate_gradients_with_ops=True)
    #         optimizer2 = tf.compat.v1.train.RMSPropOptimizer(l_rate / 10).minimize(net['cost'], var_list=net['Slow_train_vars'])
    #
    #
    #     if len(net['Fast_train_vars']) != 0 and len(net['Slow_train_vars']) != 0:
    #         train_op = tf.group(optimizer1, optimizer2)
    #
    #     if len(net['Fast_train_vars']) != 0 and len(net['Slow_train_vars']) == 0:
    #         train_op = tf.group(optimizer1)
    #
    #     if len(net['Fast_train_vars']) == 0 and len(net['Slow_train_vars']) != 0:
    #         train_op = tf.group(optimizer2)
    #
    #     config = tf.compat.v1.ConfigProto(allow_soft_placement=True)
    #     sess = tf.compat.v1.Session(config=config)
    #
    #     sess.run(tf.compat.v1.initialize_all_variables(), feed_dict={i: d for i, d in zip(net_in, model_data)})
    #
    # else:
    #     model_data = []
    #     model_shape = []
    #
    #     with h5py.File(out_dir + model_in_trueid + '/' + model_in_trueid + '.h5', 'r') as hf_in:
    #         datasets = h5bz.get_datasets(out_dir + model_in_trueid + '/' + model_in_trueid + '.h5')
    #         net_in = []
    #         filled_param_list = []
    #         for param in range(len(datasets)):
    #             h5data = hf_in.get(datasets[param])
    #             #print(datasets[param])
    #             if datasets[param] in model_vars_in:
    #                 h5data_np = np.array(h5data)
    #                 model_data.append(h5data_np)
    #                 model_shape.append(h5data_np.shape)
    #                 filled_param_list.append(param)
    #
    #
    #         net = arch.network(batch_size, precision, resolution, in_dim, h_dim, out_dim, model_in_vars=filled_param_list,
    #                            model_in_shapes=model_shape,trainable_model_in=trainable_model_in)
    #
    #         for param in range(len(filled_param_list)):
    #             net_in.append(net[datasets[param]])
    #
    #         #optimizer = tf.train.AdagradOptimizer(l_rate).minimize(net['cost'], colocate_gradients_with_ops=True)
    #         optimizer = tf.compat.v1.train.RMSPropOptimizer(l_rate).minimize(net['cost'])
    #         train_op = optimizer
    #
    #         config = tf.compat.v1.ConfigProto(allow_soft_placement=True)
    #         sess = tf.compat.v1.Session(config=config)
    #
    #         sess.run(tf.compat.v1.initialize_all_variables(), feed_dict={i: d for i, d in zip(net_in, model_data)})
    #
    # print('DONE INITIALIZING')
    # # </editor-fold>
    #
    # # <editor-fold desc="load datasets: training and test/validation">
    # # itrain_matlab = h5py.File(data_dir + train_in)
    # # print('==============')
    # # print(data_dir + train_in)
    # # print(h5bz.get_datasets(data_dir + train_in))
    #
    # if n_epochs > 0:
    #     itrain_matlab = h5py.File(data_dir + train_in, 'r')
    #     train_in_label = h5bz.get_datasets(data_dir + train_in)[0]
    #
    #     otrain_matlab = h5py.File(data_dir + train_out, 'r')
    #     train_out_label = h5bz.get_datasets(data_dir + train_out)[0]
    #
    # else:
    #     itrain_matlab = h5py.File(data_dir + test_in)
    #     train_in_label = h5bz.get_datasets(data_dir + test_in)[0]
    #
    #     otrain_matlab = h5py.File(data_dir + test_out)
    #     train_out_label = h5bz.get_datasets(data_dir + test_out)[0]
    #
    # # itrain_matlab = h5py.File(data_dir + train_in)
    # # train_in_label = h5bz.get_datasets(data_dir + train_in)[0]
    # # otrain_matlab = h5py.File(data_dir + train_out)
    # # train_out_label = h5bz.get_datasets(data_dir + train_out)[0]
    # #
    #
    #
    #
    # itest_matlab = h5py.File(data_dir + test_in, 'r')
    # test_in_label = h5bz.get_datasets(data_dir + test_in)[0]
    #
    # otest_matlab = h5py.File(data_dir + test_out, 'r')
    # test_out_label = h5bz.get_datasets(data_dir + test_out)[0]
    #
    # if precision == 'FP32':
    #     i_train = itrain_matlab[train_in_label][()]
    #     o_train = otrain_matlab[train_out_label][()]
    #     i_test = itest_matlab[test_in_label][()]
    #     o_test = otest_matlab[test_out_label][()]
    # elif precision == 'FP64':
    #     i_train = np.float64(np.array(itrain_matlab[train_in_label]))
    #     o_train = np.float64(np.array(otrain_matlab[train_out_label]))
    #     i_test = np.float64(np.array(itest_matlab[test_in_label]))
    #     o_test = np.float64(np.array(otest_matlab[test_out_label]))
    #
    #
    #
    # if i_test.shape[0] < batch_size:
    #
    #     numrep = math.ceil(batch_size/i_test.shape[0])
    #     big_test_i = np.repeat(i_test,numrep,axis=0)
    #     big_test_o = np.repeat(o_test,numrep,axis=0)
    #     test_xs = big_test_i[0:batch_size]
    #     output_xs = big_test_o[0:batch_size]
    # else:
    #     test_xs = i_test[0:batch_size]
    #     output_xs = o_test[0:batch_size]
    #
    # # </editor-fold>
    #
    # # <editor-fold desc="Prep for Batch training">
    # num_examples = i_train.shape[0]
    #
    # movavg_list = np.zeros(10)
    # valavg_list = np.zeros(5)
    # start_time = time.time()
    # total_num_batches = (num_examples // batch_size) * n_epochs
    #
    # inds = np.arange(num_examples)
    # shuf_inds = np.arange(num_examples)
    # np.random.shuffle(shuf_inds)
    # # </editor-fold>
    #
    # exp_out_dir = out_dir + trueid + '/'
    # # open log
    # full_log_filename = exp_out_dir+trueid+'.txt'
    # full_csv_filename = exp_out_dir+trueid+'.csv'
    # f = open(full_log_filename,'w+')
    # fcsv = open(full_csv_filename,'w')
    # csvwriter = csv.writer(fcsv,delimiter=' ')
    #
    # for epoch_i in range(n_epochs):
    #     np.random.shuffle(shuf_inds)
    #     for batch_i in range(num_examples // batch_size):
    #
    #         # <editor-fold desc="Batch Data Processing">
    #         batch_ind = np.arange(batch_i * batch_size, batch_i * batch_size + batch_size)
    #         shuf_batch_ind = shuf_inds[batch_ind]
    #
    #         batch_xs_i = i_train[shuf_batch_ind]
    #         batch_xs_o = o_train[shuf_batch_ind]
    #
    #         train = batch_xs_i
    #         train_targets = batch_xs_o
    #
    #         # </editor-fold>
    #
    #         # <editor-fold desc="Session Run Optimizer">
    #         sess.run([train_op],feed_dict={net['x']: train, net['y_target']: train_targets, net['keep_prob']: keep_prob,
    #                             net['corrupt_prob']: [c_prob]})
    #         # </editor-fold>
    #
    #         # <editor-fold desc="Time">
    #
    #         perc = np.round(100 * batch_i * batch_size / num_examples, 0)
    #         ctime = time.time() - start_time
    #         est_tot_time = np.round(ctime * (total_num_batches/(epoch_i * (num_examples / batch_size) + batch_i + 1)),0)
    #         # </editor-fold>
    #
    #         # <editor-fold desc="Training Cost">
    #         movavg_list = np.roll(movavg_list, 1)
    #         movavg_list[0] = sess.run(net['cost'], feed_dict={net['x']: train, net['y_target']: train_targets,
    #                                                             net['keep_prob']: keep_prob,
    #                                                             net['corrupt_prob']: [c_prob]})*(float(1) / float(batch_size * out_dim))
    #         movavg = np.average(movavg_list)
    #         # </editor-fold>
    #
    #         debug = sess.run(net['debug'], feed_dict={net['x']: test_xs, net['y_target']: output_xs,
    #                                                           net['keep_prob']: keep_prob,
    #                                                           net['corrupt_prob']: [0.0]})
    #
    #         # <editor-fold desc="Validation Cost">
    #         if batch_i % 10 == 0:
    #             valavg_list = np.roll(valavg_list, 1)
    #             valavg_list[0] = sess.run(net['valcost'],
    #                                       feed_dict={net['x']: test_xs, net['y_target']: output_xs,
    #                                                  net['keep_prob']: 1.0,
    #                                                  net['corrupt_prob']: [0.0],
    #                                                  }) * (float(1) / float(batch_size * out_dim))
    #             valavg = valavg_list[0]
    #         # </editor-fold>
    #
    #         # <editor-fold desc="Print Log in Console">
    #         print('Epoch:', epoch_i, '| Cost:', movavg, '| Valcost:', valavg, '| Debug:', debug, '| Batch:', batch_i * batch_size, '/',
    #               num_examples, int(perc), '% | Total:',
    #               int(np.rint(100 * (epoch_i * (num_examples / batch_size) + batch_i) / total_num_batches)),
    #               '% | Time:', int(est_tot_time), 's | Rem:', int(np.round(est_tot_time - ctime, 0)), 's | Elapsed:',
    #               int(ctime), 's')
    #         # </editor-fold>
    #
    #     f.write(repr(movavg) + '    ' + repr(valavg) + '\n')
    #     csvwriter.writerow(repr(epoch_i) + '    ' + repr(movavg) + '    ' + repr(valavg))
    #
    #     # WRITE TO CSV FILE
    #
    #     if epoch_i % 5 == 0:
    #
    #         # <editor-fold desc="Save Model">
    #         model_info = sess.run(net['Modelout'], feed_dict={net['x']: train, net['y_target']: train_targets,
    #                                                           net['keep_prob']: 1.0, net['corrupt_prob']: [c_prob]})
    #
    #         with h5py.File(out_dir + trueid + '/' + trueid + '.h5', 'w') as hf:
    #             for param in range(len(model_info._fields)):
    #                 hf.create_dataset(model_info._fields[param], data=getattr(model_info, model_info._fields[param]))
    #         # </editor-fold>
    #
    #         # <editor-fold desc="Save Test Output">
    #         reconimg = sess.run(net['y'], feed_dict={net['x']: test_xs, net['keep_prob']: 1.0,net['y_target']: output_xs,
    #                                                 net['corrupt_prob']: [0.0]})
    #         origimg = output_xs
    #
    #         sio.savemat(exp_out_dir+'reconimg.mat', {'reconimg': reconimg})
    #         sio.savemat(exp_out_dir+'origimg.mat', {'origimg': origimg})
    #
    #         trainreconimg = sess.run(net['y'], feed_dict={net['x']: train, net['keep_prob']: 1.0,net['y_target']: output_xs,
    #                                                           net['corrupt_prob']: [0.0]})
    #         sio.savemat(exp_out_dir + 'trainreconimg.mat', {'trainreconimg': trainreconimg})
    #         sio.savemat(exp_out_dir + 'trainorigimg.mat', {'trainrorigimg': train_targets})
    #
    #         if archname.startswith('2fc_2cnv_'):
    #             hiddenimg = sess.run(net['output_1fc'], feed_dict={net['x']: test_xs, net['keep_prob']: 1.0,net['y_target']: output_xs,
    #                                                                net['corrupt_prob']: [0.0]})
    #             preconvimg = sess.run(net['preconv'], feed_dict={net['x']: test_xs, net['keep_prob']: 1.0,net['y_target']: output_xs,
    #                                                              net['corrupt_prob']: [0.0]})
    #
    #             sio.savemat(exp_out_dir + 'hiddenimg.mat', {'hiddenimg': hiddenimg})
    #             sio.savemat(exp_out_dir + 'preconvimg.mat', {'preconvimg': preconvimg})
    #
    #
    #         if machine != 'Vulcan':
    #             server = '172.21.21.69'
    #             username = 'lfi'
    #             password = 'lfilab2.0'
    #             ssh = paramiko.SSHClient()
    #             ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")))
    #             ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    #             ssh.connect(server, username=username, password=password)
    #             sftp = ssh.open_sftp()
    #             # if no folder, create folder
    #             remote_path = scp_dir + trueid + '/'
    #             try:
    #                 sftp.chdir(remote_path)  # Test if remote_path exists
    #             except IOError:
    #                 sftp.mkdir(remote_path)  # Create remote_path
    #                 sftp.chdir(remote_path)
    #
    #             filelist = os.listdir(exp_out_dir)
    #
    #             for filenum in range(len(filelist)):
    #                 sftp.put(exp_out_dir + filelist[filenum], filelist[filenum])
    #
    #             sftp.close()
    #             ssh.close()
    #
    #     # </editor-fold>
    #
    # f.close()
    # fcsv.close()
    #
    # # <editor-fold desc="Save Model">
    #
    #
    # # <editor-fold desc="Batch Data Processing">
    # batch_i = 0
    # batch_ind = np.arange(batch_i * batch_size, batch_i * batch_size + batch_size)
    # shuf_batch_ind = shuf_inds[batch_ind]
    #
    # batch_xs_i = i_train[shuf_batch_ind]
    # batch_xs_o = o_train[shuf_batch_ind]
    #
    # train = batch_xs_i
    # train_targets = batch_xs_o
    #
    # train_cost = sess.run(net['cost'], feed_dict={net['x']: train, net['y_target']: train_targets,
    #                                  net['keep_prob']: 1.0,
    #                                  net['corrupt_prob']: [c_prob]})* (float(1) / float(batch_size * out_dim))
    #
    #
    # val_cost = sess.run(net['valcost'],
    #          feed_dict={net['x']: test_xs, net['y_target']: output_xs,
    #                     net['keep_prob']: 1.0,
    #                     net['corrupt_prob']: [0.0],
    #                     }) * (float(1) / float(batch_size * out_dim))
    #
    #
    # model_info = sess.run(net['Modelout'], feed_dict={net['x']: train, net['y_target']: train_targets,
    #                                                net['keep_prob']: 1.0, net['corrupt_prob']: [c_prob]})
    #
    # with h5py.File(out_dir + trueid + '/' + trueid + '.h5', 'w') as hf:
    #     for param in range(len(model_info._fields)):
    #         hf.create_dataset(model_info._fields[param], data=getattr(model_info,model_info._fields[param]))
    # # </editor-fold>
    #
    # # <editor-fold desc="Save Test Output">
    #
    # reconimg = sess.run(net['y'], feed_dict={net['x']: test_xs, net['keep_prob']: 1.0,net['y_target']: output_xs,
    #                                          net['corrupt_prob']: [0.0]})
    # origimg = output_xs
    # sio.savemat(exp_out_dir + 'reconimg.mat', {'reconimg': reconimg})
    # sio.savemat(exp_out_dir + 'origimg.mat', {'origimg': origimg})
    #
    # trainreconimg = sess.run(net['y'], feed_dict={net['x']: train, net['keep_prob']: 1.0,net['y_target']: output_xs,
    #                                               net['corrupt_prob']: [0.0]})
    # sio.savemat(exp_out_dir + 'trainreconimg.mat', {'trainreconimg': trainreconimg})
    # sio.savemat(exp_out_dir + 'trainorigimg.mat', {'trainrorigimg': train_targets})
    #
    # hiddenimg = sess.run(net['output_1fc'], feed_dict={net['x']: test_xs, net['keep_prob']: 1.0,net['y_target']: output_xs,
    #                                                    net['corrupt_prob']: [0.0]})
    # sio.savemat(exp_out_dir + 'hiddenimg.mat', {'hiddenimg': hiddenimg})
    #
    # if archname.startswith('2fc_2cnv_'):
    #
    #     hiddenimg = sess.run(net['output_1fc'], feed_dict={net['x']: test_xs, net['keep_prob']: 1.0,net['y_target']: output_xs,
    #                                              net['corrupt_prob']: [0.0]})
    #     preconvimg = sess.run(net['preconv'], feed_dict={net['x']: test_xs, net['keep_prob']: 1.0,net['y_target']: output_xs,
    #                                                      net['corrupt_prob']: [0.0]})
    #
    #     sio.savemat(exp_out_dir + 'hiddenimg.mat', {'hiddenimg': hiddenimg})
    #     sio.savemat(exp_out_dir + 'preconvimg.mat', {'preconvimg': preconvimg})
    #
    #
    # # </editor-fold>
    #
    #
    # if machine != 'Vulcan':
    #     server = '172.21.21.69'
    #     username = 'lfi'
    #     password = 'lfilab2.0'
    #     ssh = paramiko.SSHClient()
    #     ssh.load_host_keys(os.path.expanduser(os.path.join("~", ".ssh", "known_hosts")))
    #     ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    #     ssh.connect(server, username=username, password=password)
    #     sftp = ssh.open_sftp()
    #     # if no folder, create folder
    #     remote_path = scp_dir+trueid+'/'
    #     try:
    #         sftp.chdir(remote_path)  # Test if remote_path exists
    #     except IOError:
    #         sftp.mkdir(remote_path)  # Create remote_path
    #         sftp.chdir(remote_path)
    #
    #     filelist = os.listdir(exp_out_dir)
    #
    #     for filenum in range(len(filelist)):
    #         sftp.put(exp_out_dir+filelist[filenum], filelist[filenum])
    #
    #     sftp.close()
    #     ssh.close()
    #
    #
    # cost_return_dict = {'Train_cost':train_cost, 'Val_cost':val_cost}
    #
    # sess.close()
    # tf.compat.v1.reset_default_graph()
    print('---------SESSION CLOSED')

    return #cost_return_dict